# Tonix - [Link](https://to-nix.com)
> Fun modifications to a simple, browser based step sequencer already revived from its old flash version (RIP).

## Table of Contents
* [General Info](#general-information)
* [Technologies Used](#technologies-used)
* [Features](#features)
* [Screenshots](#screenshots)
* [Setup](#setup)
* [Project Status](#project-status)
* [Next Steps](#next-steps)
* [Acknowledgements](#acknowledgements)
* [Room for Improvement](#room-for-improvement)
* [Document Artifacts (Assignment 12)](#document-artifacts-(assignment-12))
* [Document Artifacts (Assignment 15)](#document-artifacts-(assignment-15))
* [Document Artifacts (Assignment 20)](#document-artifacts-(assignment-20))
* [Contact](#contact)
* [License](#license)


## General Information
- Tonix is an interactive step sequencer that allows users to create musical loops in an easy to use web application.
- Step sequencers are not a new concept and are prolific in the music industry, but they are often difficult to use. Most step sequencers are embedded in complicated musical production software which often requires training or a large time investment in order to reap the rewards. 
  Tonix will overcome this learning curve by giving users direct access to an easy to use step sequencer program.
- Tonix aims to put easy to use step sequence creation software into the hands of interested inviduals without scaring them off with the steep learning curve required by some elaborate music production software.
- Tonix is aimed at beginner musicians. The pentatonic scales included minimize dissonance and allow for quick creativity.
- This project was chosen by the team based on individual interests and a thourough vetting process. We also believe this project will truly develop our understanding of what it means to work on a team as a software engineer.


## Technologies Used
- https://github.com/MaxLaumeister/ToneMatrixRedux


## Features
* Playable Grid:
> - Description: 16x16 interactable grid that plays and keeps sound on clicking nodes.  
> - Story: I, Christian Solis, as a software engineer would like to make a step matrix that allows users to easily make music 
    without requiring a vast amount of musical knowledge in order to begin
* Dropdown for scale change:
> - Description: Dropdown menu to allow changing scale of the matrix/tones.  
> - Story: I, Rob, as an experienced musician, would like to select different scales to use on my simple step sequencer for
  more advanced time wasting.
* Download mp3:
> - Description: Button to save user designated number of repeat iterations.  
> - Story: I, Alejandro Alburjas, As a user of ToneMatrix Redux, I would like to save the music composed as an MP3 file.
* Good harmonizing algorithm:
> - Description: Behind-the-scenes algorithm to properly harmonize with currently activated tones.  
> - Story: I, Luke Gayler, as an avid music listener, believe we should have a diverse musical app.
* Simple UI:
> - Decription: Easy-to-use UI that a user can intuitively use.  
> - Story: I, Luke Gayler, as a person who likes functional websites, believe we should have a comprehensive website.


## Screenshots
![Tonix Logo](./images/Tonix_Logo.jpg)

## Setup
1. Install nodejs and npm: `sudo apt update; sudo apt install nodejs npm`
2. Install [Gulp CLI](https://gulpjs.com/): `sudo npm install -g gulp-cli`
3. Install php & add to PATH variable (https://www.php.net/downloads)
3. `cd` into the project folder and install dependencies: `npm install`
4. run `gulp serve` to compile and start a localhost server
5. Connect to your server with Chrome

## Project Status
Project is: _Completed_ 
> - **(Sprint 1 Completed)**
> > - Multiple UI enhancements and additions. Layout of most buttons finished. Some need further work implementing.
> - **(Sprint 2 Completed)**
> > - Accessibility options added to website. Button implementations nearly all completed. Research for features for next sprint added to BitBucket.
> - **(Sprint 3 Completed)**
> > - Added labels, persistent settings, and ability to close playback element. 

**1) Luke**:
> - **(Sprint 1)**
> > - Grid selection is functional, login/register is functional though could use improvement, and dark/color mode is functional.
> - **(Sprint 2)**
> > - Logout button is entirely functional. Added my research documents of optimizations and saving user settings to BitBucket. Website is completely accessible. Added pitch indicator.
> - **(Sprint 3)**
> > - All persistent saving features are entirely implemented within the program. Additionally, I cleaned up the functions I created to allow them to put into JS files.

**2) Rob**:
> - Mode selection functional.

**3) Alejandro**:
> - **(Sprint 1)**
> > - Colorful UI and base layout for future improvements and additions.
> - **(Sprint 2)**
> > - Help button is implemented and functional. Designed CSS for future help button pop-up window that won't be as intrusive to the user. Designed Visual Implementation in photoshop and uploaded it to the images folder. 
> - **(Sprint 3)**
> > - Researched recording pop-up functions to further improve on it. Added closing button for the recording pop-up, and implemented closing functions to eliminate pop-up from screen. 
     
 **4) Christian**:
> - **(Sprint 1)**
> > - Button to create overlay.
> - **(Sprint 2)**
> > - Functioning overlay that lays notes down over the grid to help users understand what notes they are activating when clicking on the grid. Added CSS design to overlay button. Added document with login information for google drive account and link to impliment php server access and saving. 

**5) Seth**:
> - **(Sprint 1)**
> > - Record button functional, volume slider needs functionality
> - **(Sprint 2)**
> > - Volume slider is implemented and functional with a better layout. CSS for the start and stop buttons are implemented as well as the website and domain are set up and accessible.
> - **(Sprint 3)**
> > - Recorder and volume slider are labeled and the recorder now downloads the audio as an mp3 file
## Next Steps
**1) Luke**:
> - **(Sprint 1)**
> > - Work with the team to help implement functionality of buttons.  
> > - Implement logout feature.
> - **(Sprint 2)**
> > - Design user settings saving system.
> > - Fix login/register pages to scale properly.
> - **(Sprint 3)**
> > - We're done with the sprints, but I might look into adding the account based saving on my own time!

**2) Rob**:
> - Work on refactoring scale array creation for less rigid construction of synth instrument. 
> - Solve problem with difference between default notation and scientific pitch when trying to use Tone.Frequency.harmonize()

**3) Alejandro**:
> - **(Sprint 1)**
> > - Implement help button into the UI itself.
> > - Implement dropdown for beats/minute and octave selectors.
> > - Stylize the dropdown selectors for more flair. 
> - **(Sprint 2)**
> > - Make the help window into a pop-up window to decrease its intrusiveness to the user.
> > - Improve on the visual implementation png image. Making it more clear and concise for the user.
> > - Improve the look of the login/ register pages.
> - **(Sprint 3)**
> > - Implement password requirements for login page. 
> > - Implement more features to login and registration page.

**4) Christian**:
> - **(Sprint 1)**
> > - Implement an overlapping grid into the overlay to visually express the different notes to the user.
> > - Work with the team to implement more saving features.
> - **(Sprint 2)**
> > - Refactor code to remove unnecessary bloating of index.php
> > - Fix the CSS design of overlay button and overlay to properly align with the design of the rest of the application.
> > - Work on creating an overlay that will adjust to the varying grid sizes.

**5) Seth**:
> - **(Sprint 1)**
> > - Fix the record button the run with Tone.js
> > - Add functionality for volume slider using Tone.js
> > - Research how to implement rests for each button on the matrix
> - **(Sprint 2)**
> > - Fix implementation for the record button
> > - refactor index.php so record and volume code are not in the same file
> - **(Sprint 3)**
> > - Finished all the sprints but will continue to look into making the elements on the website not clip into each other and stay static on the screen
## Acknowledgements
- This project was inspired by https://github.com/MaxLaumeister/ToneMatrixRedux
- Utilized https://www.tutorialrepublic.com/php-tutorial/php-mysql-login-system.php

## Room for Improvement
- Figure out workaround for performance issues in other browsers
- Solution to Tone.js scientific pitch and project's note names for dynamic scale creation
- More theory research and future algorithm for creating scales without hemitones or tritones
- Can improve upon button implementations.

## Document Artifacts (Assignment 12)
**1) Luke:** *Login System and Dropdown*  
> - You can find the login button backend inside of login.php. Used within index.php to give the login button functionality in order to progress towards goal of saving mp3 to account.  https://bitbucket.org/cs3398f21borgs/tonix/src/master/src/login.php
> - You can find the register button backend inside of register.php. Used within index.php to give the register button functionality in order to progress towards goal of saving mp3 to account.  https://bitbucket.org/cs3398f21borgs/tonix/src/master/src/register.php
> - Added dropdown menu script for grid size & modified ToneMatrix constructor to accommodate. Used within index.php to change size of grid so we have more musical diversity.  https://bitbucket.org/cs3398f21borgs/tonix/src/master/src/index.php (*Lines 93-122*) & https://bitbucket.org/cs3398f21borgs/tonix/src/master/src/ToneMatrix.js (*constructor*)

**2) Rob:** *Pentatonic Modes and Research*
> - OpenDocument Text explaining music theory and Tone.js findings. Used to determine what scales to implement this sprint - https://bitbucket.org/cs3398f21borgs/tonix/src/master/Theory.odt
> - Changes to the construction of the synth instrument with a switch statement based on dropdown selection - https://bitbucket.org/cs3398f21borgs/tonix/src/master/src/SynthInstrument.js
> - Additional changes to other files to implement dropdown in UI and pass the selection to SynthInstrument - https://bitbucket.org/cs3398f21borgs/tonix/commits/60da4ba6c0bbda947d6b533820f9871fce963d58

**3) Alejandro:** *UI and layout for accesability*
> - Improved on dark/drab aesthetic from the ToneMatrix Tonix is based off of by adding a changing color gradient background.
> ![CSS_code](./images/Colorful%20UI%20artifact%20code.JPG)
> - Created help button functionality that'll allow the user to get more familiar with the features in Tonix. This can be found in the HelpButton.js script.

**4) Christian:** *Overlay Button and Research*
> - Overlay Button - https://bitbucket.org/cs3398f21borgs/tonix/src/master/src/NoteOverlay.js
> - Research for saving to the cloud - https://bitbucket.org/cs3398f21borgs/tonix/src/master/Cloud%20Based%20MP3%20Saving.odt

**5) Seth:** *Record Button and Volume Slider*
> - Added the functionality for the Record Button and volume slider which can be found at the bottom of index.php where the functionality of each button is found. https://bitbucket.org/cs3398f21borgs/tonix/src/master/src/index.php
> - The code for the look and placement of these buttons are in style.css. https://bitbucket.org/cs3398f21borgs/tonix/src/master/src/style.scss

## Document Artifacts (Assignment 15)
**1) Luke:** *Logout System and Research* 
> - You can find the logout button inside of index.php(lines 5-20, 63-70, and 208-212). Used within index.php to allow the user to logout. This is necessary for our mp3 saving and user settings savings systems. https://bitbucket.org/cs3398f21borgs/tonix/src/master/src/index.php
> - You can find the research inside of "Storing User Settings.docx" and "Canvas Drawing Optimizations". Used to determine which optimizations to make and what design scheme to use for saving user settings. https://bitbucket.org/cs3398f21borgs/tonix/src/master/Canvas%20Drawing%20Optimizations.docx & https://bitbucket.org/cs3398f21borgs/tonix/src/master/Storing%20User%20Settings.docx

**2) Christian:** *Overlay Design, Overlay Button CSS, Google Drive Account Login Information*
> - Overlay design is found in lines 98-114 in https://bitbucket.org/cs3398f21borgs/tonix/src/master/src/index.php & css is found in overlay in https://bitbucket.org/cs3398f21borgs/tonix/src/master/src/style.scss
> - Overlay button css is found in overlayButton in https://bitbucket.org/cs3398f21borgs/tonix/src/master/src/style.scss
> - Google drive login information is found in https://bitbucket.org/cs3398f21borgs/tonix/src/master/CloudAccount.odt

**3) Alejandro:** *UI and layout for accesability*
> - Designed a visual implementation overlay that will go above the matrix which will make it easier for the user to understand how the matrix works. - https://bitbucket.org/cs3398f21borgs/tonix/src/master/images/visual%20implementation%20overlay.png
> - Refactored code from sprint 1 in the help button that impeded the main code in  index.php from running. 
> - Implemented help button with full functionality that'll allow the user to get more familiar with the features in Tonix. This can be found in the HelpButton.js script. - https://bitbucket.org/cs3398f21borgs/tonix/src/master/src/HelpButton.js

**4) Seth:** *Record Button Style and Volume Slider Functionality*
> - Fixed the functionality of the volume slider which is located in lines 139-142 in Grid.js, lines 147-149 in SynthInstrument.js, and lines 223-225 in ToneMatrix.js https://bitbucket.org/cs3398f21borgs/tonix/src/master/src/Grid.js, https://bitbucket.org/cs3398f21borgs/tonix/src/master/src/SynthInstrument.js, https://bitbucket.org/cs3398f21borgs/tonix/src/master/src/ToneMatrix.js
> - The code for the look and placement of record button are in style.css. https://bitbucket.org/cs3398f21borgs/tonix/src/master/src/style.scss

## Document Artifacts (Assignment 20)
**1) Luke:** *Persistent Settings* 
> - Created the functions to retrieve cookies data. This is used for our persistent setting functions so we can check if a cookie for a setting exists. https://bitbucket.org/cs3398f21borgs/tonix/commits/4a591443d1ae628a0e5f07cfee0583618bf4bc96#chg-src/cookieFunctions.js
> - Persistent saving for volume slider, grid size, pentatonic scale, and color mode. This is used so the user can be their selected settings on refresh, instead of having to set the settings each time. 
> > - Volume Slider & Grid Size : https://bitbucket.org/cs3398f21borgs/tonix/commits/4a591443d1ae628a0e5f07cfee0583618bf4bc96
> > - Pentatonic Scale: https://bitbucket.org/cs3398f21borgs/tonix/commits/f8442466a83a7855b8f6286757c51984f3af0775
> > - Color Mode: https://bitbucket.org/cs3398f21borgs/tonix/commits/ea97a6393fd10f9c61577bc70707d43c9663dec0

**2) Seth:** *UI labels and MP3 dowloads*
> - Created labels for the volume slider and recorder so the user is able to identify what they are used for. https://bitbucket.org/cs3398f21borgs/tonix/src/master/src/style.scss on lines 380-381
> - Made it to where the recorder no longer downloads the audio as a blob but now dowloads it as an mp3 file for easy use. https://bitbucket.org/cs3398f21borgs/tonix/src/master/src/style.scss on line 392

**3) Alejandro:** *User Interface* 
> - Researched blob functionality and how the media recorder is implemented into the site.
> - Created X button that eliminates the media recorder once user is done with it, so it doesn't clutter up the UI. https://bitbucket.org/cs3398f21borgs/tonix/commits/d0a4c3f03af37b0c1c108a7378bcad069255aba5

## Contact
- Created by Christian Solis, Luke Gayler, Alejandro Alburjas, Seth Petersen, Robert Balthrop.
- Email(in order): cas443@txstate.edu, lmg207@txstate.edu, a_a1218@txstate.edu, srp168@txstate.edu, r_b324@txtstate.edu


## License
This project is open source and available under the GPL-3.0 License.

