function toggleOverlay() {
  let overlayDiv = document.querySelector(".overlay");
  console.log(overlayDiv.style.opacity);
  if (overlayDiv.style.opacity === '0')
    overlayDiv.style.opacity = 1;
  else
    overlayDiv.style.opacity = 0;
}

//Credits for the button go to:
//https://stackoverflow.com/questions/55878979/how-to-create-an-overlay-on-click-of-a-button