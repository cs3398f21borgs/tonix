var dropdown2 = $('.dropdown2'),
    item2 = $('.item2'),
    selectedItem2 = item2.filter(".selected2");

$(window).on('click', function(event) {
    if (!item2.hasClass('collapse2')) {
        toggleList2();
    }
});

function toggleList2() {
    item2.toggleClass('collapse2');
    if (dropdown2.hasClass('dropped2')) {
        dropdown2.toggleClass('dropped2');
    } else {
        setTimeout(function() {
            dropdown2.toggleClass('dropped2');
        }, 150);
    }
}

item2.on('click', function (event) {
    event.stopPropagation();

    selectedItem2.text($(this).text());
    if (selectedItem2.text() !== 'Select Item')
        selectedItem2.addClass('active2');

    toggleList2();
});
