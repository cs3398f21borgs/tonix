var dropdown = $('.dropdown'),
    item = $('.item'),
    selectedItem = item.filter(".selected");

$(window).on('click', function(event) {
    if (!item.hasClass('collapse')) {
        toggleList();
    }
});

function toggleList() {
    item.toggleClass('collapse');
    if (dropdown.hasClass('dropped')) {
        dropdown.toggleClass('dropped');
    } else {
        setTimeout(function() {
            dropdown.toggleClass('dropped');
        }, 150);
    }
}

item.on('click', function(event) {
    event.stopPropagation();

    selectedItem.text($(this).text());
    if (selectedItem.text() !== 'Select Item')
        selectedItem.addClass('active');

    toggleList();
});
