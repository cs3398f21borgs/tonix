<?php
session_start();


if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true){
echo '<form method = "post" style="width: 148.5px; position: relative; top: -108.5%; right: -150%" id = "logoutForm"><input type="submit" value = "Log Out" name = "logoutButton" id="logoutButton" alt="Back" style="background-color: transparent; font-size: 1.25rem;width:148.25px;text-align: center;text-transform: uppercase;display: inline-block;padding: 0.3em 1em;text-decoration: none;color: white;border: solid 1.5px #8a2eb8;transition: .4s;" onmouseover="hoveredLogout()" onmouseout="leaveLogout()"></form>';
}

function logout()
{
$_SESSION = array();
session_destroy();
header("location: login.php");
}

if(isset($_POST['logoutButton']))
{
    logout();
}
?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <script type = "text/javascript" src = "https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script>
    </script>
	<meta name="viewport" content="width=530, user-scalable=no">
	<title>Tonix</title>
	<meta name="description" content="An improved pentatonic scale sequencer.">
	<meta name="author" content="Borgs">
	<link rel="canonical" href="https://bitbucket.org/cs3398f21borgs/tonix/src/master/">
	<link rel="manifest" href="site.webmanifest">
	<meta name="application-name" content="ToneMatrix">
	<meta name="msapplication-TileColor" content="#000000">
	<meta name="theme-color" content="#000000">
	<script>
	// If there is no trailing slash after the path in the url, add it
	// Workaround for Netlify trailing-slash rewrite issue
	// https://community.netlify.com/t/bug-in-non-trailing-slash-rewrite/452/23
	if (!window.location.pathname.endsWith('/') && !window.location.pathname.split("/").pop().includes(".")) {
		var url = window.location.protocol + '//' +
			window.location.host +
			window.location.pathname + '/' +
			window.location.search;
			window.history.replaceState(null, document.title, url);
	}
	</script>
	<link rel="stylesheet" href="style.css">
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-24440376-11"></script>
	<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'UA-24440376-11');
	</script>
  </head>
  <script>
  function hoveredLogout()
  {
    $("#logoutButton").css("background-color", "#8a2eb8");
  }
  function leaveLogout()
  {
    $("#logoutButton").css("background-color", "transparent");
  }
  </script>
  <body>


	<div class="canvaswrap" id="canvaswrap">

		<h1>Tonix</h1>
		<a class="play-btn" href="javascript:void(0)"></a>
		<div class="dropdown">
			<div class="item collapse">8x8</div>
			<div class="item collapse">16x16</div>
			<div class="item collapse">32x32</div>
			<div style="font-size: 20px" class="item collapse selected" id="selectedOption">Select Grid Size</div>
		</div>
		<div class="dropdown2">
			<div class="item2 collapse2">Major</div>
			<div class="item2 collapse2">Egypt sus</div>
			<div class="item2 collapse2">Blues min</div>
			<div class="item2 collapse2">Blues maj</div>
			<div class="item2 collapse2">Minor</div>
			<div style="font-size: 20px" class="item2 collapse2 selected2" id="selectedOption2">Pentatonic Mode</div>
		</div>



		<a href="register.php" id = "register" class="register_btn" style='font-family: Roboto Thin, sans-serif'>Register</a>
		<a href="login.php" id = "login" class="login_btn" style='font-family: Roboto Thin, sans-serif'>Login</a>
        <div class="overlay">C6,  C6, ..., C6 <br> <br>
                             A5,  A5, ..., A5 <br> <br>
                             G5,  G5, ..., G5 <br> <br>
                             F5,  F5, ..., F5 <br> <br>
                             D5,  D5, ..., D5 <br> <br>
                             C5,  C5, ..., C5 <br>
                             A4,  A4, ..., A4 <br> <br>
                             G4,  G4, ..., G4 <br> <br>
                             F4,  F4, ..., F4 <br> <br>
                             D4,  D4, ..., D4 <br> <br>
                             C4,  C4, ..., C4 <br> <br>
                             A3,  A3, ..., A3 <br> <br>
                             G3,  G3, ..., G3 <br> <br>
                             F3,  F3, ..., F3 <br> <br>
                             D3,  D3, ..., D3 <br> <br>
                             C2,  C2, ..., C2
                </div>

		<p id = "initialText"></span><a id="clearnotes" class="button" href="javascript:void(0)">Clear Notes</a><a id = "basedOnText">Based on </a> <a href="https://www.maxlaumeister.com/tonematrix/" id = "basedOn">ToneMatrix Redux</a>
                <button id=record>
                Start
                </button>
                <button id=close onclick="ClosePopUp()">
                +
                </button>
                <button id=stopRecord disabled>
                Stop
                </button>

                <button onclick="toggleOverlay()" id="overlayButton">Show/Hide Overlay</button>


                <span id="volumcontroller"></span>
        		</p>
		<script>
			var x = 16;
			var y = 16;
			var gridSelection = "";
			$('#selectedOption').bind('DOMSubtreeModified', function(){
				if($('#selectedOption').text() == "Select Grid Size")
                {
                    gridSelection = "no_selection";
                }
				else {
					if($('#selectedOption').text() == "8x8")
					{
					    gridSelection = "selection";
						x  = 8;
						y = 8;
					}
					if($('#selectedOption').text() == "16x16")
					{
					    gridSelection = "selection";
						x  = 16;
						y = 16;
					}
					if($('#selectedOption').text() == "32x32")
					{
					    gridSelection = "selection";
						x  = 32;
						y = 32;
					}
                    if(gridSelection != "no_selection" && gridSelection != "" && getCookie("pentatonicSetting") != "") //if the pentatonic has been changed
                    {
                        $("canvas").remove();
                        const toneMatrixInstance = new ToneMatrix(document.querySelector(".canvaswrap"), document.querySelector("#clearnotes"), x, y, readCookie("pentatonicSetting"), document.querySelector("#volume"));
                        toneMatrixInstance.clear();
                        var selectedSize = x;
                        document.cookie = "gridSize=" + selectedSize;
                    }
                    if(gridSelection != "no_selection" && gridSelection != "" && getCookie("pentatonicSetting") == "")
                    {
                        $("canvas").remove();
                        const toneMatrixInstance = new ToneMatrix(document.querySelector(".canvaswrap"), document.querySelector("#clearnotes"), x, y, 4, document.querySelector("#volume"));
                        toneMatrixInstance.clear();
                        var selectedSize = x;
                        document.cookie = "gridSize=" + selectedSize;
                    }
				}
			});
		</script>

		<script>
			var z = 4;
			var pentatonicSelection;
			console.log($("selectedOption2").text())
			$('#selectedOption2').bind('DOMSubtreeModified', function(){
				if($('#selectedOption2').text() == "Pentatonic Mode")
					pentatonicSelection = "no_selection";
				else {
					if($('#selectedOption2').text() == "Major")
					{
					    pentatonicSelection = "selection";
						z = 1;
					}
					if($('#selectedOption2').text() == "Egypt sus")
					{
					    pentatonicSelection = "selection";
						z = 2;
					}
					if($('#selectedOption2').text() == "Blues min")
					{
					    pentatonicSelection = "selection";
						z = 3;
					}
					if($('#selectedOption2').text() == "Blues maj")
					{
					    pentatonicSelection = "selection";
						z = 4;
					}
					if($('#selectedOption2').text() == "Minor")
					{
					    pentatonicSelection = "selection";
						z = 5;
					}
                    if(pentatonicSelection != "no_selection" && pentatonicSelection != "" && getCookie("gridSize") != "") //if grid size has been changed
                    {
						$("canvas").remove();
						const toneMatrixInstance = new ToneMatrix(document.querySelector(".canvaswrap"), document.querySelector("#clearnotes"), readCookie("gridSize"), readCookie("gridSize"), z, document.querySelector("#volume"));
						toneMatrixInstance.clear();
                        var selectedScale = z;
                        document.cookie = "pentatonicSetting=" + selectedScale;
                    }
                    if(pentatonicSelection != "no_selection" && pentatonicSelection != "" && getCookie("gridSize") == "") //if we haven't changed the grid size yet
                    {
                        $("canvas").remove();
                        const toneMatrixInstance = new ToneMatrix(document.querySelector(".canvaswrap"), document.querySelector("#clearnotes"), 16, 16, z, document.querySelector("#volume"));
                        toneMatrixInstance.clear();
                        var selectedScale = z;
                        document.cookie = "pentatonicSetting=" + selectedScale;
                    }

				}
			});
		</script>
	</div>
	<script src="all.js"></script>
	<script>
	window.onload = function ()
    {
        if (getCookie("volume"))
        {
            document.getElementById("volume").value = readCookie("volume");
        }
    }
	</script>
	<img src="arrow.png" alt="Visual Indicator" id="pitchIndicator"></img>
	<script>
    function volumeCookieChange()
    {
        var selectedVolume = document.getElementById("volume").value;
        document.cookie = "volume=" + selectedVolume;
    }
	</script>
	<input type="range" min="1" max="100" class="slider" id="volume" onchange="volumeCookieChange()">
	<script>
    if(getCookie("gridSize") != "" && getCookie("pentatonicSetting") != "") //changed both before reload
    {
        const toneMatrixInstance = new ToneMatrix(document.querySelector(".canvaswrap"), document.querySelector("#clearnotes"), readCookie("gridSize"), readCookie("gridSize"), readCookie("pentatonicSetting"), document.querySelector("#volume"));
    }
    else if(getCookie("gridSize") != "" && getCookie("pentatonicSetting") == "") //changed only the gridSize before reload
    {
        const toneMatrixInstance = new ToneMatrix(document.querySelector(".canvaswrap"), document.querySelector("#clearnotes"), readCookie("gridSize"), readCookie("gridSize"), 4, document.querySelector("#volume"));
    }
    else if(getCookie("gridSize") == "" && getCookie("pentatonicSetting") != "") //changed the pentatonicSetting before reload
    {
        const toneMatrixInstance = new ToneMatrix(document.querySelector(".canvaswrap"), document.querySelector("#clearnotes"), 16, 16, readCookie("pentatonicSetting"), document.querySelector("#volume"));
    }
    else
    {
	    const toneMatrixInstance = new ToneMatrix(document.querySelector(".canvaswrap"), document.querySelector("#clearnotes"), 16, 16, 4, document.querySelector("#volume"));
	}
	</script>
	<script>
	var appendVolume = document.getElementById("volume");
	document.getElementById("initialText").appendChild(appendVolume);
	</script>
	<script> //since we create the logout button dynamically we have to tell it where to go
	if(document.getElementById("logoutForm"))
	{
	var appendElement = document.getElementById("logoutForm");
    document.getElementById("canvaswrap").appendChild(appendElement);
    }
	</script>
	<audio id=recordedAudio></audio>
    <input type="checkbox" id="toggle" onclick="checkColor()"/>
    <label for="toggle" id="colorMode"></label>
        <script>
        function checkColor()
        {
         var checkBox = document.querySelector("#toggle");
         var h1 = document.querySelector("h1");
         var loginButton = document.querySelector("#login");
         var registerButton = document.querySelector("#register");
         if(document.getElementById("logoutForm"))
            var logout = document.querySelector("#logoutButton");
         var body = document.body;
            checkBox.addEventListener('change', () => { //this is the to set to dark gray
              if(checkBox.checked) {
                document.cookie = "colorMode=" + "dark";
                body.style.background = '#111111';
                h1.style.cssText = 'font-size: 2rem;text-transform: uppercase;letter-spacing: 2px;color: #ffffff;border: 5px solid #444444;position: absolute;top: -1.5%;left: 50%;transform: translate(-50%, -50%);padding: 0.5rem 0.5rem;margin-bottom: 0;';
                loginButton.style.cssText = 'font-size: 1.25rem;width:148.25px;text-align: center;text-transform: uppercase;display: inline-block;padding: 0.3em 1em;text-decoration: none;color: white;border: solid 1.5px #444444;transition: .4s;position: absolute;top: -8.5%;right: -60%;';
                registerButton.style.cssText = 'font-size: 1.25rem; text-transform: uppercase;text-align: center; display: inline-block;padding: 0.3em 1em;text-decoration: none;color: white;border: solid 1.5px #444444;transition: .4s;position: absolute;top: -8.5%;right: -82.5%;'
                if(document.getElementById("logoutForm"))
                {
                    logout.style.cssText = "background-color: transparent; font-size: 1.25rem;width:148.25px;text-align: center;text-transform: uppercase;display: inline-block;padding: 0.3em 1em;text-decoration: none;color: white;border: solid 1.5px #444444;transition: .4s;"
                      $(document).ready(function(){
                        $("#logoutButton").hover(function(){
                          $(this).css("background-color", "#222222");
                          }, function(){
                          $(this).css("background-color", "transparent");
                        });
                      });
                }
                  $(document).ready(function(){
                    $("#login").hover(function(){
                      $(this).css("background-color", "#222222");
                      }, function(){
                      $(this).css("background-color", "transparent");
                    });
                  });

                  $(document).ready(function(){
                      $("#register").hover(function(){
                        $(this).css("background-color", "#222222");
                        }, function(){
                        $(this).css("background-color", "transparent");
                      });
                    });
             }
              else {
                document.cookie = "colorMode=" + "color";
                body.style.cssText = 'background: linear-gradient(-45deg, #EE7752, #E73C7E, #23A6D5, #23D5AB); background-size: 400% 400%; font-family: "Lato", Arial, sans-serif;margin: 0;min-height: 100%;width: 100%;position: relative;animation: change 20s ease-in-out infinite;';
                h1.style.cssText = 'font-size: 2rem;text-transform: uppercase;letter-spacing: 2px;color: #ffffff;border: 5px solid #8a2eb8;position: absolute;top: -1.5%;left: 50%;transform: translate(-50%, -50%);padding: 0.5rem 0.5rem;margin-bottom: 0;';
                loginButton.style.cssText = 'font-size: 1.25rem;width:148.25px;text-align: center;text-transform: uppercase;display: inline-block;padding: 0.3em 1em;text-decoration: none;color: white;border: solid 1.5px #8a2eb8;transition: .4s;position: absolute;top: -8.5%;right: -60%;';
                registerButton.style.cssText = 'font-size: 1.25rem; text-transform: uppercase;text-align: center; display: inline-block;padding: 0.3em 1em;text-decoration: none;color: white;border: solid 1.5px #8a2eb8;transition: .4s;position: absolute;top: -8.5%;right: -82.5%;'

                if(document.getElementById("logoutForm"))
                {
                    logout.style.cssText = "background-color: transparent; font-size: 1.25rem;width:148.25px;text-align: center;text-transform: uppercase;display: inline-block;padding: 0.3em 1em;text-decoration: none;color: white;border: solid 1.5px #8a2eb8;transition: .4s;"
                      $(document).ready(function(){
                        $("#logoutButton").hover(function(){
                          $(this).css("background-color", "#8a2eb8");
                          }, function(){
                          $(this).css("background-color", "transparent");
                        });
                      });
                }
                  $(document).ready(function(){
                    $("#login").hover(function(){
                      $(this).css("background-color", "#8a2eb8");
                      }, function(){
                      $(this).css("background-color", "transparent");
                    });
                  });

                  $(document).ready(function(){
                      $("#register").hover(function(){
                        $(this).css("background-color", "#8a2eb8");
                        }, function(){
                        $(this).css("background-color", "transparent");
                      });
                    });
              }
            });
        }
        </script>
        <script>
            if(getCookie("colorMode") != "")
            {
                if(getCookie("colorMode") == "dark")
                {
                    var toggleBox = document.querySelector("#toggle");
                    toggleBox.click();
                    checkColor();
                }
                else if(getCookie("colorMode") == "color")
                    checkColor();
            }

        </script>
        <h3>Recorder</h3>
        <h4>Volume</h4>
           <script>
           navigator.mediaDevices.getUserMedia({audio:true})
           .then(stream => {handlerFunction(stream)})


            function handlerFunction(stream) {
                rec = new MediaRecorder(stream);
                rec.ondataavailable = e => {
                    audioChunks.push(e.data);
                    if (rec.state == "inactive"){
                        let recordPopUp = new Blob(audioChunks,{type:'audio/mpeg'});
                        recordedAudio.src = URL.createObjectURL(recordPopUp);
                        recordedAudio.controls=true;
                        recordedAudio.autoplay=true;
                        sendData(recordPopUp)
                    }
                };
            }
            function sendData(data){}

            record.onclick = e => {
                console.log('I was clicked')
                record.disabled = true;
                record.style.backgroundColor = "green"
                stopRecord.style.backgroundColor = "green"
                stopRecord.disabled=false;
                audioChunks = [];
                rec.start();
            }
            stopRecord.onclick = e => {
                console.log("I was clicked")
                record.disabled = false;
                stop.disabled=true;
                record.style.backgroundColor = "red"
                stopRecord.style.backgroundColor = "red"
                rec.stop();
                document.getElementById("recordedAudio").style.visibility = "visible";
                document.getElementById("close").style.visibility = "visible";
            }

            function ClosePopUp(){
            document.getElementById("recordedAudio").style.visibility = "hidden";
            document.getElementById("close").style.visibility = "hidden";
            }




            </script>

         <a id="help-button" class= "button" onclick="displayHelp()"> Help</a>

         <div class = "bg-modal">
            <div class = "modal-content">
                To get started creating music, you can click on any individual box
                    a pentatonic tone will play. You may adjust the volume at the bottom of the page,
                    save your progress, and change the grid itself to your liking.

                <div class="close">+</div>

            </div>
         </div>
  </body>
</html>
