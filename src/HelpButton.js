//  Help button function to display information for the user

function displayHelp() {
   alert('To get started creating music, you can click on any individual box\n'
  + 'and a pentatonic tone will play. You may adjust the volume at the bottom of the page,\n'
  + 'save your progress, and change the grid itself to your liking');

};
